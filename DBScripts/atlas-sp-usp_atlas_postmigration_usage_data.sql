-- exec [usp_atlas_postmigration_usage_data]
IF EXISTS (SELECT * FROM sys.procedures	WHERE name = 'usp_atlas_postmigration_usage_data')
	DROP PROCEDURE usp_atlas_postmigration_usage_data
GO
CREATE PROC [dbo].[usp_atlas_postmigration_usage_data] (@Mig_groupName varchar(100) = 'All Companies')
AS

BEGIN

	DECLARE @Migration_groupName varchar(100)
	SET @Migration_groupName = @Mig_groupName

	DECLARE @MIN int
	DECLARE @MAX int
	DECLARE @CompanyID uniqueidentifier
	DECLARE @Date datetime


	IF (@Migration_groupName = 'All Companies')
	BEGIN
		SET @MIN = (SELECT MIN(ID) FROM Stage_Migrated_company_List)
		SET @MAX = (SELECT MAX(ID) FROM Stage_Migrated_company_List)
	END
	ELSE
	BEGIN
		SET @MIN = (SELECT MIN(ID) FROM Stage_Migrated_company_List WHERE Migration_groupName = @Migration_groupName)
		SET @MAX = (SELECT MAX(ID)FROM Stage_Migrated_company_List WHERE Migration_groupName = @Migration_groupName)
	END
	WHILE (@MIN <= @MAX)
	BEGIN
		SET @CompanyID = (SELECT ATLASCOMPANYID FROM Stage_Migrated_company_List WHERE id = @MIN)
		SET @Date = (SELECT Migrated_Date FROM Stage_Migrated_company_List WHERE id = @MIN)
		SET @Migration_groupName = (SELECT Migration_groupName FROM Stage_Migrated_company_List WHERE id = @MIN)
		INSERT INTO [atlas_postmigration_usage_details] ([CompanyID], [SalesforceAccountID], [CompanyName], [URL_Password_changed]
					, [Logged_into_Atlas], TermsConditionsAccepted, [Riskassessments_Pending], [Riskassessments_Live], [Riskassessments_New]
					, [Employees_New], [Users_New], [Traininggrowth], [Superuser_inactive], [CaseVolumes_Total], [CaseVolumes_Closed]
					, [HasClientContacted], [HasClientLoggedIn], [HasClientTrained], [Documentaccesscount], [Documentdistributioncount]
					, [Migration_groupName], [MigratedDate], [ExecutedTime], [migration_executive])

			SELECT
				ID AS CompanyID,
				SalesforceAccountID,
				ShortName CompanyName,
				ISNULL(URL_Password_changed, 0) URL_Password_changed,
				CASE WHEN ISNULL(Logged_into_Atlas, 0) >= 1 THEN 1 ELSE 0 END AS Logged_into_Atlas,
				CASE WHEN ISNULL(Terms_Conditions_Accepted, 0) >= 1 THEN 1 ELSE 0 END AS Terms_Conditions_Accepted,
				ISNULL(Riskassessments_Pending, 0) Riskassessments_Pending,
				ISNULL(Riskassessments_Live, 0) Riskassessments_Live,
				ISNULL(Riskassessments_New, 0) Riskassessments_New,
				ISNULL(Employees_New, 0) Employees_New,
				ISNULL(Users_New, 0) Users_New,
				ISNULL(Traininggrowth, 0) Traininggrowth,
				ISNULL(Superuser_inactive, 0) Superuser_inactive,
				ISNULL(CaseVolumes_Total, 0) CaseVolumes_Total,
				ISNULL(CaseVolumes_Closed, 0) CaseVolumes_Closed,
				ISNULL(HasClientContacted, 0) HasClientContacted,
				ISNULL(HasClientLoggedIn, 0) HasClientLoggedIn,
				ISNULL(HasClientTrained, 0) HasClientTrained,
				ISNULL(Documentaccesscount, 0) Documentaccesscount,
				ISNULL(Documentdistributioncount, 0) Documentdistributioncount,
				@Migration_groupName Migration_groupName,
				@Date MigratedDate,
				GETDATE() ExecutedTime,
				Migration_Executive
			FROM citation.companies a
			LEFT JOIN (SELECT
							companyID,
							COUNT(CASE WHEN URL_Password_changed = 1 THEN 1 END) AS URL_Password_changed,
							SUM(Logged_into_Atlas) Logged_into_Atlas,
							SUM(Terms_Conditions_Accepted) Terms_Conditions_Accepted,
							COUNT(CASE WHEN isactive = 0 THEN 1 END) Superuser_inactive
						FROM (SELECT
									u.companyID,
									u.id,
									CASE WHEN ui.Password = '' THEN 0 ELSE 1 END AS URL_Password_changed,
									CASE WHEN ISNULL(ui.TcAccepted, 0) = 1 THEN 1 ELSE 0 END AS Terms_Conditions_Accepted,
									CASE WHEN ui.LastLoggedInTime IS NOT NULL THEN 1 ELSE 0 END AS Logged_into_Atlas,
									u.isactive
								FROM shared.Usersidentity ui
								INNER JOIN (SELECT
												u.id,
												u.companyID,
												u.isactive
											FROM shared.users u
											INNER JOIN userprofilesmap upm
											ON	u.id = upm.Userid
											AND UserprofileID IN ('ED8F7D9B-DFFD-49EC-8934-5327D2BF1AA4', '1078A9A4-101E-4B22-AF96-446AA7C5C793', 'EFD4D143-48C1-471C-B8BA-B91ADBC9B156')
											INNER JOIN UserProfiles up
											ON up.id = upm.UserprofileID
											WHERE u.companyID = @CompanyID
											GROUP BY	u.id,
														u.companyID,
														u.isactive) u
								ON u.id = ui.id
								AND ui.modifiedOn > @Date) a
						GROUP BY companyID) b
			ON a.ID = b.CompanYID
			--RAS Pending
			LEFT JOIN (SELECT
				companyID,
				COUNT(*) Riskassessments_Pending
			FROM riskassessments
			WHERE companyID = @CompanyID
			AND modifiedOn > @Date
			AND CreatedOn <> modifiedOn
			AND Statusid = 1
			GROUP BY companyID) c
				ON a.ID = c.CompanYID
			-- RAS Live
			LEFT JOIN (SELECT
				companyID,
				COUNT(*) Riskassessments_Live
			FROM riskassessments
			WHERE companyID = @CompanyID
			AND modifiedOn > @Date
			AND CreatedOn <> modifiedOn
			AND Statusid = 2
			GROUP BY companyID) d
				ON a.ID = d.CompanYID
			-- RAS Added
			LEFT JOIN (SELECT
				companyID,
				COUNT(*) Riskassessments_New
			FROM riskassessments
			WHERE companyID = @CompanyID
			AND Createdon > @Date
			GROUP BY companyID) e
				ON a.ID = e.CompanYID
			-- Employees Added
			LEFT JOIN (SELECT
				companyID,
				COUNT(*) Employees_New
			FROM Employees
			WHERE companyID = @CompanyID
			AND Createdon > @Date
			GROUP BY companyID) f
				ON a.ID = e.CompanYID
			--Users Added
			LEFT JOIN (SELECT
				companyID,
				COUNT(*) Users_New
			FROM shared.users
			WHERE CompanyID = @CompanyID
			AND Createdon > @Date
			GROUP BY companyID) g
				ON a.ID = g.CompanYID
			--Trainings Added
			LEFT JOIN (SELECT
				companyID,
				COUNT(*) Traininggrowth
			FROM shared.TrainingUserCourseModule
			WHERE CompanyID = @CompanyID
			AND Createdon > @Date
			AND CreatedBy <> 'D84DD1C4-8615-4A42-858C-423E6D90DE7C'
			GROUP BY companyID) h
				ON a.ID = h.CompanYID

			-- CaseVolumes_Total
			LEFT JOIN (SELECT
				cc.id CompanYID,
				COUNT(issueid) CaseVolumes_Total
			FROM citation.companies cc
			LEFT JOIN incidents.hdusers hu
				ON cc.jitbitcompanyid = hu.companyID
			LEFT JOIN incidents.hdissues hi
				ON hu.userid = hi.Userid
				AND lastupdated > @Date
			WHERE CC.id = @CompanyID
			GROUP BY cc.id) i
				ON a.ID = i.CompanYID

			--CaseVolumes_Closed
			LEFT JOIN (SELECT
				cc.id CompanYID,
				COUNT(issueid) CaseVolumes_Closed
			FROM citation.companies cc
			LEFT JOIN incidents.hdusers hu
				ON cc.jitbitcompanyid = hu.companyID
			LEFT JOIN incidents.hdissues hi
				ON hu.userid = hi.Userid
				AND statusid = 3
				AND lastupdated > @Date
			WHERE CC.id = @CompanyID
			GROUP BY cc.id) j
				ON a.ID = j.CompanYID

			-- Clients details
			LEFT JOIN (SELECT
				CompanyID,
				SUM(HasClientContacted) HasClientContacted,
				SUM(HasClientLoggedIn) HasClientLoggedIn,
				SUM(HasClientTrained) HasClientTrained
			FROM (SELECT
				cc.id CompanyID,
				CASE
					WHEN ih.FieldID = 52 THEN 1
					ELSE 0
				END AS HasClientContacted,
				CASE
					WHEN ih.FieldID = 53 THEN 1
					ELSE 0
				END AS HasClientLoggedIn,
				CASE
					WHEN ih.FieldID = 54 THEN 1
					ELSE 0
				END AS HasClientTrained
			FROM citation.companies cc
			INNER JOIN Stage_Migrated_company_List mcl
				ON mcl.ATLASCOMPANYID = cc.Id
			INNER JOIN incidents.hdCustomFieldValues ih
				ON mcl.issueid = ih.issueID
			WHERE CC.id = @CompanyID
			AND ih.fieldid IN (52, 53, 54)) a
			GROUP BY CompanyID) k
				ON a.ID = k.CompanYID

			-- Documentaccesscount
			LEFT JOIN (SELECT
				CompanyID,
				COUNT(*) AS Documentaccesscount
			FROM ActionedDocuments
			WHERE CompanyID = @CompanyID
			AND actiontakenon > @Date
			GROUP BY CompanyID) l
				ON a.ID = l.CompanYID


			-- DocumentDistributedCOunt
			LEFT JOIN (SELECT
				CompanyID,
				COUNT(*) Documentdistributioncount
			FROM ( --outer Query
			SELECT DISTINCT
				COmpanyID,
				EmployeeID
			FROM ( --inner Query
			-------------------------------------------------------------------------------------------------------------------------
			--Employee-17
			SELECT DISTINCT
				COmpanyID,
				EmployeeID
			FROM (SELECT
				dds.companyid,
				dds.id,
				dds.documentid,
				regardingobjecttypecode,
				dd.regardingobjectid,
				e.id AS EmployeeID
			FROM DistributedDocuments dds
			INNER JOIN DistributedDocumentDetails dd
				ON dd.id = dds.id
				AND dds.CreatedOn > @Date
			INNER JOIN employees e
				ON dd.regardingobjectid = e.id
				AND e.isleaver = 0 -- isactive removed in R1.11 so changed condition to isLeaver =0
			WHERE dds.companyid = @CompanyID
			AND regardingobjecttypecode = 17) emp
			UNION ALL
			-------------------------------------------------------------------------------------------------------------------------
			--Company -1
			SELECT DISTINCT
				COmpanyID,
				EmployeeID
			FROM (SELECT
				dds.companyid,
				dds.id,
				dds.documentid,
				regardingobjecttypecode,
				dd.regardingobjectid,
				e.id AS EmployeeID
			FROM DistributedDocuments dds
			INNER JOIN DistributedDocumentDetails dd
				ON dd.id = dds.id
				AND dds.CreatedOn > @Date
			INNER JOIN employees e
				ON dd.regardingobjectid = e.CompanyID
				AND e.isleaver = 0 -- isactive removed in R1.11 so changed condition to isLeaver =0
			WHERE dds.companyid = @CompanyID
			AND regardingobjecttypecode = 1) comp

			UNION ALL
			-------------------------------------------------------------------------------------------------------------------------
			--EMployeegroup -4018

			SELECT DISTINCT
				COmpanyID,
				EmployeeID
			FROM (SELECT
				dds.companyid,
				dds.id,
				dds.documentid,
				regardingobjecttypecode,
				dd.regardingobjectid,
				ega.Employeeid
			FROM DistributedDocuments dds
			INNER JOIN DistributedDocumentDetails dd
				ON dd.id = dds.id
				AND dds.CreatedOn > @Date
			INNER JOIN EmployeeGroup eg
				ON eg.id = dd.regardingobjectid
			INNER JOIN EmployeeGroupAssociation ega
				ON ega.employeegroupid = eg.id
			INNER JOIN employees e
				ON ega.Employeeid = e.id
				AND e.isleaver = 0 -- isactive removed in R1.11 so changed condition to isLeaver =0
			WHERE dds.companyid = @CompanyID
			AND regardingobjecttypecode = 4018) egroup
			UNION ALL
			-------------------------------------------------------------------------------------------------------------------------
			--Sites
			SELECT DISTINCT
				COmpanyID,
				EmployeeID
			FROM (SELECT
				dds.companyid,
				dds.id,
				dds.documentid,
				regardingobjecttypecode,
				dd.regardingobjectid,
				j.Employeeid
			FROM DistributedDocuments dds
			INNER JOIN DistributedDocumentDetails dd
				ON dd.id = dds.id
				AND dds.CreatedOn > @Date
			INNER JOIN jobs j
				ON j.siteid = dd.regardingobjectid
			INNER JOIN employees e
				ON j.Employeeid = e.id
				AND e.isleaver = 0 -- isactive removed in R1.11 so changed condition to isLeaver =0
			WHERE dds.companyid = @CompanyID
			AND regardingobjecttypecode = 3 --AND dd.regardingobjectid='CFFEFC22-4820-40DD-9A53-0B774F3D0CF4'
			) sites
			-------------------------------------------------------------------------------------------------------------------------
			UNION ALL
			--Department-4
			SELECT DISTINCT
				COmpanyID,
				EmployeeID
			FROM (SELECT
				dds.companyid,
				dds.id,
				dds.documentid,
				regardingobjecttypecode,
				dd.regardingobjectid,
				j.DepartmentID,
				e.id AS EmployeeID
			FROM DistributedDocuments dds
			INNER JOIN DistributedDocumentDetails dd
				ON dd.id = dds.id
				AND dds.CreatedOn > @Date
			INNER JOIN departments d
				ON dd.regardingobjectid = d.id
			INNER JOIN jobs j
				ON j.DepartmentID = d.id
			INNER JOIN employees e
				ON j.Employeeid = e.id
				AND e.isleaver = 0 -- isactive removed in R1.11 so changed condition to isLeaver =0
			WHERE dds.companyid = @CompanyID
			AND regardingobjecttypecode = 4) Dept) innerQuery) outerQuery

			GROUP BY CompanyID) m
				ON a.ID = m.CompanYID


			LEFT JOIN (SELECT		-- hdi.IssueId, hdi.AssignedtoUserId, 
				mcl.AtlasCompanyId CompanyID,
				ISNULL(u.FirstName, '') + ' ' + ISNULL(u.SecondName, '') AS Migration_Executive
			FROM Incidents.hdIssues hdi
			INNER JOIN Stage_Migrated_company_List mcl
				ON hdi.IssueId = mcl.IssueId
			INNER JOIN Incidents.hdUsers hdu
				ON hdu.UserId = hdi.AssignedToUserId
			INNER JOIN Shared.Users u
				ON u.Id = hdu.AtlasUserId
			WHERE hdi.AssignedToUserid IS NOT NULL
			AND mcl.atlascompanyid = @CompanyID) n
				ON a.ID = n.CompanYID


			WHERE ID = @CompanyID

		SET @MIN = @MIN + 1

	END
END